"use strict";

// entity
let Filter = 
exports.Filter = class Filter {
  constructor(something) {
    this.something = something;
  }
}

// parser (instantiates an instance from ABP syntax string)
exports.Filter.fromText = function(text) {
    let jsonObj = JSON.parse(text);
    let newFilter = new Filter(jsonObj.something);
    return newFilter;
};
