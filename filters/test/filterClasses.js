"use strict";

var assert = require('assert');
const { Filter } = require("../lib/filterClasses");

describe("Filter parser", function() {
    it("returns object field", function() {
        assert.equal("test123", Filter.fromText('{ "something": "test123" }').something);
    });
});