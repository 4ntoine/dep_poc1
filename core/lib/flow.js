"use strict";

let { Filter } = require("filters");

let Flow =
exports.Flow = class Flow {
  constructor(converter) {
    this.converter = converter;
  }

  doSomething(text) {
    let abpFilter = Filter.fromText(text);
    // assuming there is an agreement on `convertor` interface:
    // eg. `convert()` that accepts filter class instance and returns string/anything
    return this.converter.convert(abpFilter);
  }
}